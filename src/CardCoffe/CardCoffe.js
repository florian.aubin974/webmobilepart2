import React, {useState} from 'react';
import '../App.css';

function CardCoffe() {

    function importAll(r) {
        return r.keys().map(r);
    }

    const images = importAll(require.context('./../images', false, /\.(png|jpe?g|svg)$/));

    const [coffees] = useState([
        { name: "Perspiciatis", image: images[0] },
        { name: "Voluptatem", image: images[1] },
        { name: "Explicabo", image: images[2] },
        { name: "Rchitecto", image: images[3] },
        { name: "Beatae", image: images[4] },
        { name: "Vitae", image: images[5] },
        { name: "Inventore", image: images[6] },
        { name: "Veritatis", image: images[7] },
        { name: "Accusantium", image: images[8] },
    ])

    //cards

    return (
        <div className="container">
            {coffees.map((coffe) => (
                <div className="card">
                    <img className="card--avatar" src={coffe.image}  alt={"vide"}/>
                    <h1 className="card--title">{coffe.name}</h1>
                    <a className="card--link" href="#">Taste</a>
                </div>
            ))}
        </div>
    );
}

export default CardCoffe;
