import React from 'react';
import './App.css';
import CardCoffe from "./CardCoffe/CardCoffe";

function App() {
    return (
        <div>
            <nav>
                <h1>Dev'Coffee</h1>
                <ul>
                    <li>Home</li>
                    <li>About</li>
                    <li>Blog</li>
                </ul>
            </nav>
            <CardCoffe/>
        </div>
    );
}

export default App;
